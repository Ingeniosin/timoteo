using DynamicApi.EntityFramework;
using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Academicos;
using Timoteo.Models.Calendarios;
using Timoteo.Models.Cursos;
using Timoteo.Models.Generales;
using Timoteo.Models.Matriculas;
using Timoteo.Models.Notas;
using Timoteo.Models.Programas;
using Timoteo.Models.Semestres;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models;

public class ApplicationDbContext : DynamicContext {

    public ApplicationDbContext(DbContextOptions options) : base(options) {
    }
    
    //EstadoAcademico.cs, InformacionAcademica, Calendario, TipoCalendario, Curso, Aula, Ciudad,Configuracion, DiaSemana, Matricula, Actividad, FaltaAsistencia, Nota, CursoPrograma, Programa,CursoSemestre,  Semestre, Rol, TipoIdentificacion, Usuario
    
    public DbSet<EstadoAcademico> EstadosAcademicos { get; set; }
    public DbSet<InformacionAcademica> InformacionesAcademicas { get; set; }
    public DbSet<Calendario> Calendarios { get; set; }
    public DbSet<TipoCalendario> TiposCalendarios { get; set; }
    public DbSet<Curso> Cursos { get; set; }
    public DbSet<Aula> Aulas { get; set; }
    public DbSet<Ciudad> Ciudades { get; set; }
    public DbSet<Configuracion> Configuraciones { get; set; }
    public DbSet<DiaSemana> DiasSemana { get; set; }
    public DbSet<Matricula> Matriculas { get; set; }
    public DbSet<Actividad> Actividades { get; set; }
    public DbSet<FaltaAsistencia> FaltaAsistencias { get; set; }
    public DbSet<Nota> Notas { get; set; }
    public DbSet<CursoPrograma> CursosProgramas { get; set; }
    public DbSet<Programa> Programas { get; set; }
    public DbSet<CursoSemestre> CursosSemestres { get; set; }
    public DbSet<Semestre> Semestres { get; set; }
    public DbSet<Rol> Roles { get; set; }
    public DbSet<TipoIdentificacion> TiposIdentificacion { get; set; }
    public DbSet<Usuario> Usuarios { get; set; }
    
}