using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models.Notas;

[Index(nameof(EstudianteId), nameof(ActividadId), IsUnique = true)]
public class Nota {

    public int Id { get; set; }

    public virtual Actividad Actividad { get; set; }
    public int ActividadId { get; set; }

    public virtual Usuario Estudiante { get; set; }
    public int EstudianteId { get; set; }

    public decimal Valor { get; set; }
    public DateTime Fecha { get; set; }

}