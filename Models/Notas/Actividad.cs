using System.ComponentModel.DataAnnotations;
using Timoteo.Models.Semestres;

namespace Timoteo.Models.Notas;

public class Actividad {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public string Descripcion { get; set; }

    public decimal? Porcentaje { get; set; }

    public virtual CursoSemestre CursoSemestre { get; set; }
    public int CursoSemestreId { get; set; }

}