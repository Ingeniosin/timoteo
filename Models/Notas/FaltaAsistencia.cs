using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Semestres;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models.Notas;

[Index(nameof(EstudianteId), nameof(CursoSemestreId), nameof(Fecha), IsUnique = true)]
public class FaltaAsistencia {

    public int Id { get; set; }

    public virtual Usuario Estudiante { get; set; }
    public int EstudianteId { get; set; }

    public virtual CursoSemestre CursoSemestre { get; set; }
    public int CursoSemestreId { get; set; }

    [Column(TypeName = "Date")] public DateTime Fecha { get; set; }

}