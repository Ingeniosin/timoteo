using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Semestres;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models.Matriculas;

[Index(nameof(UsuarioId), nameof(CursoSemestreId), IsUnique = true)]
public class Matricula {

    public int Id { get; set; }

    public virtual Usuario Usuario { get; set; }
    public int UsuarioId { get; set; }

    public virtual CursoSemestre CursoSemestre { get; set; }
    public int CursoSemestreId { get; set; }

    public DateTime FechaMatricula { get; set; }

}