using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Programas;

[Index(nameof(Codigo), IsUnique = true)]
public class Programa {

    public int Id { get; set; }

    [Required] public string Codigo { get; set; }

    [Required] public string Nombre { get; set; }

}