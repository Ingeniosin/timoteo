using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Cursos;

namespace Timoteo.Models.Programas;

[Index(nameof(CursoId), nameof(ProgramaId), IsUnique = true)]
public class CursoPrograma {

    public int Id { get; set; }

    public virtual Curso Curso { get; set; }
    public int CursoId { get; set; }

    public virtual Programa Programa { get; set; }
    public int ProgramaId { get; set; }

    public int NumeroSemestre { get; set; }
    public int Creditos { get; set; }

}