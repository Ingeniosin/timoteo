using Timoteo.Models.Programas;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models.Academicos;

public class InformacionAcademica {

    public int Id { get; set; }

    public virtual Usuario Usuario { get; set; }
    public int UsuarioId { get; set; }

    public virtual Programa Programa { get; set; }
    public int ProgramaId { get; set; }

    public virtual EstadoAcademico EstadoAcademico { get; set; }
    public int EstadoAcademicoId { get; set; }

    public DateTime FechaIngreso { get; set; }

}