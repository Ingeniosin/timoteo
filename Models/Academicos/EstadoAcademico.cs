using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Academicos;

[Index(nameof(Nombre), IsUnique = true)]
public class EstadoAcademico {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public bool EsNormal { get; set; }
    public bool EsSuspendido { get; set; }
    public bool EsRetirado { get; set; }

    public static List<EstadoAcademico> DefaultValues(ApplicationDbContext db) {
        return new List<EstadoAcademico> {
            new() {
                Id = 1,
                Nombre = "Normal",
                EsNormal = true,
                EsSuspendido = false,
                EsRetirado = false
            },
            new() {
                Id = 2,
                Nombre = "Suspendido",
                EsNormal = false,
                EsSuspendido = true,
                EsRetirado = false
            },
            new() {
                Id = 3,
                Nombre = "Retirado",
                EsNormal = false,
                EsSuspendido = false,
                EsRetirado = true
            }
        };
    }

}