using System.ComponentModel.DataAnnotations;

namespace Timoteo.Models.Usuarios;

public class Rol {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public bool EsEstudiante { get; set; }
    public bool EsProfesor { get; set; }
    public bool EsAdministrador { get; set; }

    public static List<Rol> DefaultValues(ApplicationDbContext db) {
        return new List<Rol> { new() { Id = 1, Nombre = "Estudiante", EsEstudiante = true }, new() { Id = 2, Nombre = "Profesor", EsProfesor = true }, new() { Id = 3, Nombre = "Administrador", EsAdministrador = true } };
    }

}