using System.ComponentModel.DataAnnotations;
using DynamicApi.Configurations;
using Microsoft.EntityFrameworkCore;
using Timoteo.Models.Generales;

namespace Timoteo.Models.Usuarios;

[Index(nameof(NumeroIdentificacion), nameof(TipoIdentificacionId), IsUnique = true)]
[Index(nameof(Email), IsUnique = true)]
[Index(nameof(IdentificacionInstitucion), IsUnique = true)]
public class Usuario {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    [Required] public string Apellido { get; set; }

    [Required] public string Email { get; set; }

    [Required] 
    [JsonIgnoreGet]
    public string Password { get; set; }

    [Required] public string NumeroIdentificacion { get; set; }

    [Required] public string IdentificacionInstitucion { get; set; }

    public virtual TipoIdentificacion TipoIdentificacion { get; set; }
    public int TipoIdentificacionId { get; set; }

    public virtual Rol Rol { get; set; }
    public int RolId { get; set; }

    public virtual Ciudad Ciudad { get; set; }
    public int CiudadId { get; set; }

    public static List<Usuario> DefaultValues(ApplicationDbContext db) {
        return new List<Usuario> {
            new Usuario { 
                Id = 1, 
                Nombre = "Admin", 
                Apellido = "",
                Email = "admin@gmail.com",
                Password = "$2b$10$cwIhuGXpekVdGeOp8EUPJu/s2AbOncH6Z85mdEng3TWe2O8Q7y1j2", //123
                NumeroIdentificacion = "123456789",
                IdentificacionInstitucion = "123456789",
                TipoIdentificacionId = 1,
                RolId = 3,
                CiudadId = 1
            }
        };
    }
}