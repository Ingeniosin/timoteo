using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Usuarios;

[Index(nameof(Nombre), IsUnique = true)]
public class TipoIdentificacion {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public bool EsCedula { get; set; }
    public bool EsTarjetaIdentidad { get; set; }

    public static List<TipoIdentificacion> DefaultValues(ApplicationDbContext db) {
        return new List<TipoIdentificacion> { new() { Id = 1, Nombre = "Cédula de Ciudadanía", EsCedula = true }, new() { Id = 2, Nombre = "Tarjeta de Identidad", EsTarjetaIdentidad = true } };
    }

}