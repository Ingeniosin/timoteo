using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Calendarios;

[Index(nameof(Nombre), IsUnique = true)]
public class TipoCalendario {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public bool EsInscripcionMatricula { get; set; }
    public bool EsBajaSinPenalidad { get; set; }
    public bool EsBajaConPenalidad { get; set; }

    public static List<TipoCalendario> DefaultValues(ApplicationDbContext db) {
        return new List<TipoCalendario> { new() { Id = 1, Nombre = "Inscripción", EsInscripcionMatricula = true }, new() { Id = 2, Nombre = "Baja sin penalidad", EsBajaSinPenalidad = true }, new() { Id = 3, Nombre = "Baja con penalidad", EsBajaConPenalidad = true } };
    }

}