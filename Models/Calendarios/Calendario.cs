namespace Timoteo.Models.Calendarios;

public class Calendario {

    public int Id { get; set; }
    public DateTime FechaInicio { get; set; }
    public DateTime FechaFin { get; set; }

    public virtual TipoCalendario TipoCalendario { get; set; }
    public int TipoCalendarioId { get; set; }

}