using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Cursos;

[Index(nameof(Codigo), IsUnique = true)]
public class Curso {

    public int Id { get; set; }

    [Required] public string Codigo { get; set; }

    [Required] public string Nombre { get; set; }

    public string Descripcion { get; set; }
    public int HorasSemanales { get; set; }

}