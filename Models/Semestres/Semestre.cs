using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Semestres;

[Index(nameof(Codigo), IsUnique = true)]
public class Semestre {

    public int Id { get; set; }

    [Required] public string Codigo { get; set; }

    public DateTime FechaInicio { get; set; }
    public DateTime FechaFin { get; set; }

}