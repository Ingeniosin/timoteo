using Timoteo.Models.Cursos;
using Timoteo.Models.Generales;
using Timoteo.Models.Usuarios;

namespace Timoteo.Models.Semestres;

public class CursoSemestre {

    public int Id { get; set; }

    public virtual Semestre Semestre { get; set; }
    public int SemestreId { get; set; }

    public virtual Curso Curso { get; set; }
    public int CursoId { get; set; }

    public virtual Ciudad Ciudad { get; set; }
    public int CiudadId { get; set; }

    public virtual Usuario Docente { get; set; }
    public int? DocenteId { get; set; }

    public virtual Aula Aula { get; set; }
    public int? AulaId { get; set; }

    public virtual DiaSemana DiaSemana { get; set; }
    public int DiaSemanaId { get; set; }

    public TimeSpan HoraInicio { get; set; }
    public TimeSpan HoraFin { get; set; }

}