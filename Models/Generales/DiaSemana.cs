using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Generales;

[Index(nameof(Nombre), IsUnique = true)]
[Index(nameof(NumeroDia), IsUnique = true)]
public class DiaSemana {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public int NumeroDia { get; set; }

    public static List<DiaSemana> DefaultValues(ApplicationDbContext db) {
        return new List<DiaSemana> {
            new() { Id = 1, Nombre = "Lunes", NumeroDia = 1 },
            new() { Id = 2, Nombre = "Martes", NumeroDia = 2 },
            new() { Id = 3, Nombre = "Miercoles", NumeroDia = 3 },
            new() { Id = 4, Nombre = "Jueves", NumeroDia = 4 },
            new() { Id = 5, Nombre = "Viernes", NumeroDia = 5 },
            new() { Id = 6, Nombre = "Sabado", NumeroDia = 6 },
            new() { Id = 7, Nombre = "Domingo", NumeroDia = 7 }
        };
    }

}