using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Generales;

[Index(nameof(Nombre), nameof(CiudadId), IsUnique = true)]
public class Aula {

    public int Id { get; set; }

    [Required] public string Nombre { get; set; }

    public virtual Ciudad Ciudad { get; set; }
    public int CiudadId { get; set; }

    public static List<Aula> DefaultValues(ApplicationDbContext db) {
        return new List<Aula> {
            new() { Id = 1, Nombre = "Aula informatica 301", CiudadId = 1 },
            new() { Id = 2, Nombre = "Aula informatica 302", CiudadId = 1 },
            new() { Id = 3, Nombre = "Aula informatica 303", CiudadId = 1 },
            new() { Id = 4, Nombre = "Aula informatica 304", CiudadId = 1 },
            new() { Id = 5, Nombre = "Aula 401", CiudadId = 1 },
            new() { Id = 6, Nombre = "Aula 402", CiudadId = 1 },
            new() { Id = 7, Nombre = "Aula 403", CiudadId = 1 },
            new() { Id = 8, Nombre = "Aula 404", CiudadId = 1 }
        };
    }

}