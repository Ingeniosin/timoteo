using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Timoteo.Models.Generales;

[Index(nameof(Codigo), IsUnique = true)]
public class Ciudad {

    public int Id { get; set; }

    [Required] public string Codigo { get; set; }

    [Required] public string Nombre { get; set; }

    public static List<Ciudad> DefaultValues(ApplicationDbContext db) {
        return new List<Ciudad> {
            new() { Id = 1, Nombre = "Pasto", Codigo = "001" },
            new() { Id = 2, Nombre = "Cúcuta", Codigo = "002" },
            new() { Id = 3, Nombre = "Bogotá", Codigo = "003" },
            new() { Id = 4, Nombre = "Medellín", Codigo = "004" },
            new() { Id = 5, Nombre = "Cali", Codigo = "005" },
            new() { Id = 6, Nombre = "Barranquilla", Codigo = "006" },
            new() { Id = 7, Nombre = "Cartagena", Codigo = "007" },
            new() { Id = 8, Nombre = "Bucaramanga", Codigo = "008" }
        };
    }

}