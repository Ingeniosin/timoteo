namespace Timoteo.Models.Generales;

public class Configuracion {

    public int Id { get; set; }
    public int MaximoFaltas { get; set; }

    public static List<Configuracion> DefaultValues(ApplicationDbContext db) {
        return new List<Configuracion> { new() { Id = 1, MaximoFaltas = 4 } };
    }

}