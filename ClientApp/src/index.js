import React from 'react'
import { createRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import App from './App'
import { Toaster } from 'react-hot-toast'
import load from './configuration/dxConfig'

import 'devextreme/dist/css/dx.light.css'
import './index.css'

const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href')
const rootElement = document.getElementById('root')
const root = createRoot(rootElement)

load()

root.render(
  <BrowserRouter basename={baseUrl}>
    <App />
    <Toaster position='bottom-right' />
  </BrowserRouter>
)
