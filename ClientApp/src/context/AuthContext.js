import { createContext, useState } from 'react'

const AuthContext = createContext()

const KEY = 'userData'

const AuthContextWrapper = ({ children }) => {
  const [userData, setUserData] = useState(localStorage.getItem(KEY) ? JSON.parse(localStorage.getItem(KEY)) : null)
  return (
    <AuthContext.Provider value={
     {
       userData,
       setUserData: (data) => {
         localStorage.setItem(KEY, JSON.stringify(data))
         setUserData(data)
       },
       isLogged: userData !== null,
       nombreCompleto: userData?.nombre + ' ' + userData?.apellido,
       logout: () => {
         localStorage.removeItem(KEY)
         setUserData(null)
       },
       isAdmin: userData?.rolId === 3,
       isTeacher: userData?.rolId === 2,
       isStudent: userData?.rolId === 1
     }
    }
    >
      {children}
    </AuthContext.Provider>
  )
}

export {
  AuthContext,
  AuthContextWrapper
}
