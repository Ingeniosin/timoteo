import AppRouter from './routers/AppRouter'
import { AuthContextWrapper } from './context/AuthContext'

const App = () => {
  return (
    <AuthContextWrapper>
      <AppRouter />
    </AuthContextWrapper>
  )
}

export default App
