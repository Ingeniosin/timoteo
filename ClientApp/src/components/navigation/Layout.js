import Navbar from './Navbar'
import Aside from './Aside'
import { Outlet } from 'react-router-dom'

const heightContent = {
  height: 'calc(100vh - 5rem)',
  width: 'calc(100vw - 16rem)'
}

const Layout = () => {
  return (
    <>
      <Navbar />

      <div className='flex gap-3 w-full h-full'>
        <Aside />
        <div id='content' className='flex flex-row border-t w-full max-w-full overflow-y-visible overflow-y-scroll p-5 bg-[#fafafa]' style={heightContent}>
          <Outlet />
        </div>
      </div>
    </>
  )
}

export default Layout
