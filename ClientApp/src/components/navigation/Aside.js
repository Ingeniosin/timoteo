import { Link } from 'react-router-dom'
import { AuthContext } from '../../context/AuthContext'
import { useContext } from 'react'

const Aside = () => {
  const { isAdmin, isTeacher, isStudent } = useContext(AuthContext)

  return (
    <aside
      className='flex flex-col w-64 px-5 overflow-y-auto bg-white border-r rtl:border-r-0 rtl:border-l dark:bg-gray-900 dark:border-gray-700'
      style={{
        height: 'calc(100vh - 5rem)'
      }}
    >
      <div className='flex flex-col justify-between flex-1 mt-6'>
        <nav className='-mx-3 space-y-6 '>

          {
            isAdmin && (
              <div className='space-y-3 '>
                <label className='px-3 text-xs text-gray-500 uppercase dark:text-gray-400'>Administración</label>

                <CustomLink to='/admin/usuarios' icon='book' title='Gestion de usuarios'>
                  <i class='fas fa-users' />
                </CustomLink>
                <CustomLink to='/admin/academica' icon='book' title='Gestion academica'>
                  <i class='fas fa-graduation-cap' />
                </CustomLink>
                <CustomLink to='/admin/calendario' icon='book' title='Gestion de calendarios'>
                  <i class='fas fa-calendar-check' />
                </CustomLink>
                <CustomLink to='/admin/semestre' icon='book' title='Gestion de semestres'>
                  <i class='far fa-calendar-alt' />
                </CustomLink>
                <CustomLink to='/admin/oferta' icon='book' title='Gestion oferta academica'>
                  <i class='fas fa-th' />
                </CustomLink>
              </div>
            )
          }

          {
            (isAdmin || isTeacher) && (
              <div className='space-y-3 '>
                <label className='px-3 text-xs text-gray-500 uppercase dark:text-gray-400'>Docente</label>
                <CustomLink to='/profesor/cursos' icon='book' title='Mis cursos'>
                  <i class='fas fa-book' />
                </CustomLink>
              </div>
            )
          }

          {
            (isAdmin || isStudent) && (
              <div className='space-y-3 '>
                <label className='px-3 text-xs text-gray-500 uppercase dark:text-gray-400'>Estudiante</label>
                <CustomLink to='/estudiante/cursos' icon='book' title='Mi pensum'>
                  <i class='fas fa-list' />
                </CustomLink>
              </div>
            )
          }
        </nav>
      </div>
    </aside>

  )
}

const CustomLink = ({ to, children, title }) => {
  return (
    <Link to={to} className='flex items-center px-3 py-2 text-gray-600 transition-colors duration-300 transform rounded-lg dark:text-gray-200 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:text-gray-200 hover:text-gray-700'>
      {children}
      <span className='mx-2 text-sm font-medium'>{title}</span>
    </Link>
  )
}

export default Aside
