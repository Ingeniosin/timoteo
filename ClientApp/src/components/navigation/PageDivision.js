const PageDivision = ({ title, children }) => {
  return (
    <div className='p-5 2xl:col-span-3'>
      <h1 className='text-2xl font-bold mb-6'>{title}</h1>
      {children}
    </div>
  )
}

export default PageDivision
