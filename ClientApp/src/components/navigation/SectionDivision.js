const SectionDivision = ({ children, title, size = 'xl' }) => {
  return (
    <div className='rounded-3xl shadow-xl p-5 2xl:col-span-3 bg-white'>
      <h1 className={'text-' + size + ' font-bold mb-3'}>{title}</h1>
      {children}
    </div>
  )
}

export default SectionDivision
