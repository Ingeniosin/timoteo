import { DataGrid, Column, Paging } from 'devextreme-react/data-grid'
import { getDs } from '../../api/api-service'
import React from 'react'

const dsSemestres = getDs('semestres')

const SemestresGrid = () => {
  return (
    <DataGrid dataSource={dsSemestres}>
      <Column dataField='codigo' dataType='text' />
      <Column dataField='fechaInicio' dataType='date' />
      <Column dataField='fechaFin' dataType='date' />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default SemestresGrid
