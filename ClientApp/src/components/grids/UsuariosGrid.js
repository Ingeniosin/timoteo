import { getDs, getDsOptions } from '../../api/api-service'
import { Column, DataGrid, Lookup, MasterDetail, Paging } from 'devextreme-react/data-grid'
import SectionDivision from '../navigation/SectionDivision'
import React from 'react'

const dsUsuarios = getDs('usuarios')
const dsCiudades = getDs('ciudades')
const dsRoles = getDs('roles')
const dsTipoIdentificacion = getDs('tiposIdentificacion')
const dsProgramas = getDs('programas')
const dsEstadosAcademicos = getDs('estadosAcademicos')

const passwordSettings = { mode: 'password' }

const DetailUsuarios = ({ data: master }) => {
  const { data } = master
  const dsInfoAcademica = getDsOptions('InformacionesAcademicas', {
    filter: ['UsuarioId', '=', data.id]
  })

  return (
    <SectionDivision title={'Información academica: ' + data.nombre + ' ' + data.apellido} size='sm'>
      <DataGrid
        dataSource={dsInfoAcademica}
        onInitNewRow={(e) => {
          e.data.usuarioId = data.id
        }}
      >
        <Column dataField='programaId' caption='Programa'>
          <Lookup dataSource={dsProgramas} valueExpr='id' displayExpr='nombre' />
        </Column>
        <Column dataField='estadoAcademicoId'>
          <Lookup dataSource={dsEstadosAcademicos} valueExpr='id' displayExpr='nombre' />
        </Column>
        <Column dataField='fechaIngreso' dataType='date' />
        <Paging defaultPageSize={10} />
      </DataGrid>
    </SectionDivision>

  )
}

const UsuariosGrid = () => {
  return (
    <DataGrid dataSource={dsUsuarios}>
      <Column dataField='nombre' />
      <Column dataField='apellido' />
      <Column dataField='email' />
      <Column dataField='numeroIdentificacion' />
      <Column dataField='identificacionInstitucion' />
      <Column dataField='tipoIdentificacionId'>
        <Lookup dataSource={dsTipoIdentificacion} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='rolId' caption='Rol'>
        <Lookup dataSource={dsRoles} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='ciudadId' caption='Ciudad'>
        <Lookup dataSource={dsCiudades} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='password' caption='Nueva contraseña' showEditorAlways editorOptions={passwordSettings} allowExporting={false} allowGrouping={false} />
      <MasterDetail enabled component={DetailUsuarios} />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default UsuariosGrid
