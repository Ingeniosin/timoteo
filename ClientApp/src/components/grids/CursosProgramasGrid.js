import { getDs } from '../../api/api-service'
import { DataGrid, Column, Lookup, Paging } from 'devextreme-react/data-grid'
import React from 'react'

const dsCursos = getDs('cursos')
const dsProgramas = getDs('programas')
const dsCursosProgramas = getDs('cursosProgramas')

const CursosProgramasGrid = () => {
  return (
    <DataGrid dataSource={dsCursosProgramas}>

      <Column dataField='cursoId' caption='Curso'>
        <Lookup dataSource={dsCursos} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='programaId' caption='Programa'>
        <Lookup dataSource={dsProgramas} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='numeroSemestre' dataType='number' />
      <Column dataField='credito' dataType='number' />

      <Paging defaultPageSize={10} />

    </DataGrid>
  )
}

export default CursosProgramasGrid
