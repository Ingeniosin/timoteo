import { getDs, getDsOptions } from '../../api/api-service'
import { Column, DataGrid, Lookup, MasterDetail, Paging } from 'devextreme-react/data-grid'
import React from 'react'
import SectionDivision from '../navigation/SectionDivision'

const dsCursos = getDs('cursos')
const dsProgramas = getDs('programas')

const DetailProgramas = ({ data: master }) => {
  const { data } = master
  const dsCursosProgramas = getDsOptions('cursosprogramas', {
    filter: ['programaId', '=', data.id]
  })

  return (
    <SectionDivision title={'Cursos del programa: [' + data.codigo + '] ' + data.nombre} size='sm'>
      <DataGrid
        dataSource={dsCursosProgramas}
        onInitNewRow={(e) => {
          e.data.programaId = data.id
        }}
      >
        <Column dataField='cursoId' caption='Curso'>
          <Lookup dataSource={dsCursos} valueExpr='id' displayExpr='nombre' />
        </Column>
        <Column dataField='numeroSemestre' dataType='number' />
        <Column dataField='creditos' dataType='number' />
        <Paging defaultPageSize={10} />
      </DataGrid>
    </SectionDivision>

  )
}

const ProgramasGrid = () => {
  return (
    <DataGrid dataSource={dsProgramas}>
      <Column dataField='codigo' />
      <Column dataField='nombre' />
      <MasterDetail enabled component={DetailProgramas} />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default ProgramasGrid
