import { getDs } from '../../api/api-service'
import { Column, DataGrid, Lookup, Paging } from 'devextreme-react/data-grid'

const dsCalendario = getDs('calendarios')
const dsTipoCalendario = getDs('tiposCalendarios')

const CalendarioGrid = () => {
  return (
    <DataGrid dataSource={dsCalendario}>
      <Column dataField='fechaInicio' dataType='date' />
      <Column dataField='fechaFin' dataType='date' />
      <Column dataField='tipoCalendarioId'>
        <Lookup dataSource={dsTipoCalendario} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default CalendarioGrid
