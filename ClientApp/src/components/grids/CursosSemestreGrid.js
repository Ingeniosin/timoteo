import { getDs, getDsOptionsLookup } from '../../api/api-service'
import { Column, DataGrid, Lookup, Paging } from 'devextreme-react/data-grid'
import React from 'react'

const dsCursosSemestre = getDs('CursosSemestres')
const dsDiasSemana = getDs('DiasSemana')
const dsUsuarios = getDsOptionsLookup('Usuarios', {
  filter: [['rolId', '=', 2], 'or', ['rolId', '=', 3]]
})
const dsCiudades = getDs('Ciudades')
const dsCursos = getDs('Cursos')
const dsSemestres = getDs('Semestres')

const editorOptions = {
  type: 'time',
  displayFormat: 'HH:mm',

  calendarOptions: {
    visible: false
  }
}

const CursosSemestreGrid = () => {
  return (
    <DataGrid dataSource={dsCursosSemestre}>
      <Column dataField='semestreId'>
        <Lookup dataSource={dsSemestres} valueExpr='id' displayExpr='codigo' />
      </Column>
      <Column dataField='cursoId'>
        <Lookup dataSource={dsCursos} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='ciudadId'>
        <Lookup dataSource={dsCiudades} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='docenteId'>
        <Lookup dataSource={dsUsuarios} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='diaSemanaId'>
        <Lookup dataSource={dsDiasSemana} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='horaInicio' dataType='datetime' format='shortTime' editorOptions={editorOptions} />
      <Column dataField='horaFin' dataType='datetime' format='shortTime' editorOptions={editorOptions} />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default CursosSemestreGrid
