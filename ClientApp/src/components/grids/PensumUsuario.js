/* eslint-disable multiline-ternary */
import { getDsOptions } from '../../api/api-service'
import { useEffect, useState } from 'react'

const dsCursosProgramas = getDsOptions('CursosProgramas', {
  select: ['*', 'curso', 'programa']
})

const PensumUsuario = ({ defaultProgram }) => {
  const [data, setData] = useState(null)
  const [selectedProgramaId, setSelectedProgramaId] = useState(defaultProgram)

  const handleProgramaIdChange = (event) => {
    setSelectedProgramaId(parseInt(event.target.value))
  }

  useEffect(() => {
    const fetchData = async () => {
      const response = await dsCursosProgramas.load()
      setData(response)
    }

    fetchData()
  }, [])

  if (!data) {
    return <div>cargando</div>
  }

  const filteredData = selectedProgramaId ? data.filter(item => item.programa.id === selectedProgramaId) : data

  const programOptions = Object.values(
    data.reduce((acc, item) => {
      acc[item.programa.id] = item.programa
      return acc
    }, {})
  ).map(program => (
    <option key={program.id} value={program.id}>
      {program.nombre}
    </option>
  ))
  const semestersData = filteredData.reduce((acc, item) => {
    const semester = item.numeroSemestre
    if (!acc[semester]) {
      acc[semester] = []
    }
    acc[semester].push(item)
    return acc
  }, {})

  return (
    <>
      {
        !defaultProgram && (
          <>
            <select
              id='programaId'
              value={selectedProgramaId}
              onChange={handleProgramaIdChange}
              className='my-2 p-2 border border-gray-300 rounded shadow-md'
            >
              <option value=''>Seleccione un programa </option>
              {programOptions}
            </select>
            <label htmlFor='cursoId' className='block mt-4 text-gray-700 font-bold'>
              Pensum del curso:
            </label>
          </>
        )
      }

      <div className='flex gap-4'>
        {selectedProgramaId && filteredData ? (
          Object.entries(semestersData).map(([semester, semesterData]) => (
            <div
              key={semester}
              className='my-4 p-4 border border-gray-300 rounded shadow-xl w-72 bg-slate-100 hover:bg-slate-300'
            >
              <h2 className='text-xl font-bold mb-2'>Semestre {semester}</h2>
              {semesterData.map((item, index) => (
                <div key={index} className='my-2'>
                  <h3 className='text-lg font-bold mb-1'>
                    Nombre: {item.curso.nombre}
                  </h3>
                  <p>
                    <strong>Horas Semanales:</strong> {item.curso.horasSemanales}
                  </p>

                  <p>
                    <strong>Programa:</strong> {item.programa.nombre}
                  </p>
                  <p>
                    <strong>Descripcion:</strong> {item.curso.descripcion}
                  </p>
                  <hr className='my-2 border border-gray-300' />
                </div>
              ))}
            </div>
          ))
        ) : (
          <div className='text-gray-600'>No se ha seleccionado ningún programa.</div>
        )}
      </div>
    </>
  )
}
export default PensumUsuario
