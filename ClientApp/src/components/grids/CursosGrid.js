import { getDs } from '../../api/api-service'
import { Column, DataGrid, Paging } from 'devextreme-react/data-grid'

const dsCursos = getDs('cursos')

const CursosGrid = () => {
  return (
    <DataGrid dataSource={dsCursos}>
      <Column dataField='codigo' />
      <Column dataField='nombre' />
      <Column dataField='descripcion' />
      <Column dataField='horasSemanales' />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default CursosGrid
