import { getDs } from '../../api/api-service'
import { Column, DataGrid, Editing, Lookup, Paging } from 'devextreme-react/data-grid'
import React, { useContext, useMemo } from 'react'
import { AuthContext } from '../../context/AuthContext'

const dsDiasSemana = getDs('DiasSemana')
const dsCiudades = getDs('Ciudades')
const dsCursos = getDs('cursos')
const dsSemestres = getDs('Semestres')

const editorOptions = {
  type: 'datetime',
  displayFormat: 'HH:mm',

  calendarOptions: {
    visible: false
  }
}

const CursosProfesorGrid = () => {
  const { userData } = useContext(AuthContext)

  const dsCursosSemestre = useMemo(() => getDs('CursosSemestres', {
    filter: ['docenteId', '=', userData.id]
  }), [])

  return (
    <DataGrid dataSource={dsCursosSemestre}>
      <Column dataField='semestreId'>
        <Lookup dataSource={dsSemestres} valueExpr='id' displayExpr='codigo' />
      </Column>
      <Column dataField='cursoId'>
        <Lookup dataSource={dsCursos} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='ciudadId'>
        <Lookup dataSource={dsCiudades} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='diaSemanaId'>
        <Lookup dataSource={dsDiasSemana} valueExpr='id' displayExpr='nombre' />
      </Column>
      <Column dataField='horaInicio' dataType='datetime' editorOptions={editorOptions} showEditorAlways />
      <Column dataField='horaFin' dataType='datetime' editorOptions={editorOptions} showEditorAlways />
      <Editing allowAdding={false} allowDeleting={false} allowUpdating={false} />
      <Paging defaultPageSize={10} />
    </DataGrid>
  )
}

export default CursosProfesorGrid
