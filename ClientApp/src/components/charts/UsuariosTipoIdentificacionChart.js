import { Label, Legend, PieChart, Series } from 'devextreme-react/pie-chart'
import useEnum from '../../hooks/useEnum'
import { getDsOptions } from '../../api/api-service'
import { useMemo } from 'react'

const UsuariosTipoIdentificacionChart = () => {
  const { isLoaded, lookup } = useEnum('tiposIdentificacion')

  const dsUsuarios = useMemo(() => getDsOptions('usuarios', {
    group: [{ selector: 'TipoIdentificacionId', isExpanded: false }],

    postProcess: (data) => {
      for (const item of data) {
        item.nombre = lookup(item.key).nombre
      }

      return data
    }

  }), [lookup])

  if (!isLoaded) return null

  return (
    <PieChart dataSource={dsUsuarios}>
      <Series argumentField='nombre' valueField='count'>
        <Label visible />
      </Series>
      <Legend horizontalAlignment='center' verticalAlignment='bottom' />
    </PieChart>
  )
}

export default UsuariosTipoIdentificacionChart
