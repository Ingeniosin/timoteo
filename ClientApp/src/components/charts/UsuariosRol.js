import { Label, Legend, Series } from 'devextreme-react/pie-chart'
import useEnum from '../../hooks/useEnum'
import { getDsOptions } from '../../api/api-service'
import { useMemo } from 'react'
import { Chart } from 'devextreme-react/chart'

const UsuariosTipoIdentificacionChart = () => {
  const { isLoaded, lookup } = useEnum('roles')

  const dsUsuarios = useMemo(() => getDsOptions('usuarios', {
    group: [{ selector: 'RolId', isExpanded: false }],

    postProcess: (data) => {
      for (const item of data) {
        item.nombre = lookup(item.key).nombre
      }

      return data
    }

  }), [lookup])

  if (!isLoaded) return null

  return (
    <Chart dataSource={dsUsuarios}>
      <Series argumentField='nombre' valueField='count'>
        <Label visible />
      </Series>
      <Legend horizontalAlignment='center' verticalAlignment='bottom' />
    </Chart>
  )
}

export default UsuariosTipoIdentificacionChart
