import { ButtonItem, EmptyItem, Form, RequiredRule, SimpleItem } from 'devextreme-react/form'
import { useContext, useMemo, useRef } from 'react'
import { action } from '../../api/api-service'
import toast from 'react-hot-toast'
import { AuthContext } from '../../context/AuthContext'

const buildSettings = (formRef, setUserData) => {
  const form = () => formRef.current.instance
  return {
    email: {
      label: {
        text: 'Correo electrónico'
      }
    },
    password: {
      label: {
        text: 'Contraseña'
      },
      options: {
        mode: 'password',
        buttons: [{
          name: 'password',
          location: 'after',
          options: {
            icon: 'eye',
            type: 'default',
            onClick: () => {
              const editor = form().getEditor('password')
              const isText = editor.option('mode') === 'text'
              editor.option('mode', isText ? 'password' : 'text')
            }
          }
        }]
      }
    },
    button: {
      options: {
        text: 'Iniciar sesión',
        type: 'default',
        useSubmitBehavior: true,
        onClick: async () => {
          const data = form().option('formData')
          const { user } = await toast.promise(action('login', data), {
            loading: 'Iniciando sesión...',
            success: ({ user }) => 'Bienvenido de nuevo ' + user.nombre + ' ' + user.apellido,
            error: 'Error al iniciar sesión, por favor verifique sus credenciales'
          })
          setUserData(user)
        }
      }
    }
  }
}

const LoginForm = () => {
  const form = useRef(null)
  const { setUserData } = useContext(AuthContext)
  const settings = useMemo(() => buildSettings(form, setUserData), [setUserData])

  return (
    <Form ref={form}>
      <SimpleItem dataField='email' label={settings.email.label}>
        <RequiredRule message='El correo electrónico es requerido' />
      </SimpleItem>
      <SimpleItem dataField='password' editorOptions={settings.password.options} label={settings.password.label}>
        <RequiredRule message='La contraseña es requerida' />
      </SimpleItem>
      <EmptyItem />
      <EmptyItem />
      <ButtonItem horizontalAlignment='center' buttonOptions={settings.button.options} />
    </Form>
  )
}

export default LoginForm
