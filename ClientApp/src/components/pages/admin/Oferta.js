import PageDivision from '../../navigation/PageDivision'
import SectionDivision from '../../navigation/SectionDivision'
import CursosSemestreGrid from '../../grids/CursosSemestreGrid'

const Oferta = () => {
  return (
    <PageDivision title='Gestion oferta academica'>

      <SectionDivision title='Oferta academica'>

        <CursosSemestreGrid />

      </SectionDivision>

    </PageDivision>
  )
}

export default Oferta
