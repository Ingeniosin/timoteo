import PageDivision from '../../navigation/PageDivision'
import SectionDivision from '../../navigation/SectionDivision'
import CalendarioGrid from '../../grids/CalendarioGrid'

const Calendarios = () => {
  return (
    <PageDivision title='Gestion de calendarios'>

      <SectionDivision title='Calendarios en el sistema'>

        <CalendarioGrid />

      </SectionDivision>

    </PageDivision>
  )
}

export default Calendarios
