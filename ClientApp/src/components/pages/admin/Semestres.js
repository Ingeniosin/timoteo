import PageDivision from '../../navigation/PageDivision'
import SectionDivision from '../../navigation/SectionDivision'
import SemestresGrid from '../../grids/SemestresGrid'

const Semestres = () => {
  return (
    <PageDivision title='Gestion de semestres'>

      <SectionDivision title='Semestres en el sistema'>

        <SemestresGrid />

      </SectionDivision>

    </PageDivision>
  )
}

export default Semestres
