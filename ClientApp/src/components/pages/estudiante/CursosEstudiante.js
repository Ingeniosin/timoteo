import PageDivision from '../../navigation/PageDivision'
import SectionDivision from '../../navigation/SectionDivision'
import PensumUsuario from '../../grids/PensumUsuario'
import { getDsOptions } from '../../../api/api-service'
import { AuthContext } from '../../../context/AuthContext'
import { useContext, useEffect, useState } from 'react'

const CursosEstudiante = () => {
  const { userData } = useContext(AuthContext)
  const [data, setData] = useState(null)

  useEffect(() => {
    getDsOptions('InformacionesAcademicas', {
      filter: ['UsuarioId', '=', userData.id]
    }).load().then(x => setData(x[0]))
  }, [])

  if (!data) {
    return <div>cargando</div>
  }

  console.log(data)

  return (
    <PageDivision title='Mis cursos'>
      <SectionDivision title='Mi Pensum'>
        <PensumUsuario defaultProgram={data.programaId} />
      </SectionDivision>
    </PageDivision>
  )
}

export default CursosEstudiante
