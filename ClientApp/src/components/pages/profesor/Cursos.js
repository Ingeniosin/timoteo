import PageDivision from '../../navigation/PageDivision'
import SectionDivision from '../../navigation/SectionDivision'
import CursosProfesorGrid from '../../grids/CursosProfesorGrid'

const Cursos = () => {
  return (
    <PageDivision title='Mis cursos'>

      <SectionDivision title='Cursos del docente'>

        <CursosProfesorGrid />

      </SectionDivision>

    </PageDivision>
  )
}

export default Cursos
