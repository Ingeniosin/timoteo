import UsuariosGrid from '../../grids/UsuariosGrid'
import UsuariosTipoIdentificacionChart from '../../charts/UsuariosTipoIdentificacionChart'
import UsuariosCiudadChart from '../../charts/UsuariosCiudadChart'
import UsuariosRol from '../../charts/UsuariosRol'

const Usuarios = () => {
  return (
    <>

      <div className='grid gap-5 2xl:grid-cols-4 grid-cols-1'>

        <div className='rounded-3xl shadow-md p-5 2xl:col-span-3'>
          <h1 className='text-2xl font-bold'>Usuarios en el sistema</h1>
          <UsuariosGrid />
        </div>

        <div className='rounded-3xl shadow-md p-6'>
          <h3 className='text-xl text-center font-bold my-5'>Estudiantes por ciudad</h3>
          <UsuariosCiudadChart />
        </div>

        <div className='rounded-3xl shadow-md p-6'>
          <h3 className='text-xl text-center font-bold my-5'>Estudiantes por tipos de identificación</h3>
          <UsuariosTipoIdentificacionChart />
        </div>

        <div className='rounded-3xl shadow-md p-6'>
          <h3 className='text-xl text-center font-bold my-5'>Estudiantes por rol</h3>
          <UsuariosRol />
        </div>

      </div>
    </>
  )
}

export default Usuarios
