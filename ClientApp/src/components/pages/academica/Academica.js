import CursosGrid from '../../grids/CursosGrid'
import ProgramasGrid from '../../grids/ProgramasGrid'
import PensumUsuario from '../../grids/PensumUsuario'
import SectionDivision from '../../navigation/SectionDivision'
import PageDivision from '../../navigation/PageDivision'

const Academica = () => {
  return (
    <PageDivision title='Gestion academica'>
      <div className='grid grid-cols-5 w-full gap-10'>
        <div className='col-span-2 gap-5 flex flex-col'>
          <SectionDivision title='Cursos'>
            <CursosGrid />
          </SectionDivision>
          <SectionDivision title='Pensum por programa'>
            <PensumUsuario />
          </SectionDivision>
        </div>
        <div className='col-span-3'>
          <SectionDivision title='Programas'>
            <ProgramasGrid />
          </SectionDivision>
        </div>
      </div>
    </PageDivision>
  )
}

export default Academica
