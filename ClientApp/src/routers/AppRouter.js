import { Navigate, Route, Routes } from 'react-router-dom'
import Layout from '../components/navigation/Layout'
import Login from '../components/pages/login/Login'
import ConditionalRoute from '../components/utils/ConditionalRoute'
import Usuarios from '../components/pages/usuarios/Usuarios'
import NotFound from '../components/utils/NotFound'
import HomePage from '../components/pages/dash/HomePage'
import { useContext } from 'react'
import { AuthContext } from '../context/AuthContext'
import Academica from '../components/pages/academica/Academica'
import Calendarios from '../components/pages/admin/Calendarios'
import Semestres from '../components/pages/admin/Semestres'
import Oferta from '../components/pages/admin/Oferta'
import Cursos from '../components/pages/profesor/Cursos'
import CursosEstudiante from '../components/pages/estudiante/CursosEstudiante'
const AppRouter = () => {
  const { isLogged, isAdmin, isTeacher, isStudent } = useContext(AuthContext)

  return (
    <Routes>
      <Route path='/*' element={<ConditionalRoute condition={isLogged} component={<Layout />} eject='/auth/login' />}>
        <Route index element={<HomePage />} />

        <Route path='admin/*' element={<ConditionalRoute condition={isAdmin} eject='/' />}>
          <Route path='usuarios' element={<Usuarios />} />
          <Route path='academica' element={<Academica />} />
          <Route path='calendario' element={<Calendarios />} />
          <Route path='semestre' element={<Semestres />} />
          <Route path='oferta' element={<Oferta />} />
        </Route>

        <Route path='profesor/*' element={<ConditionalRoute condition={(isTeacher || isAdmin)} eject='/' />}>
          <Route path='cursos' element={<Cursos />} />
        </Route>

        <Route path='estudiante/*' element={<ConditionalRoute condition={(isStudent || isAdmin)} eject='/' />}>
          <Route path='cursos' element={<CursosEstudiante />} />
        </Route>

        <Route path='*' element={<Navigate to='/404' />} />
      </Route>
      <Route path='/auth/*' element={<ConditionalRoute condition={!isLogged} eject='/' />}>
        <Route path='login' element={<Login />} />

        <Route path='*' element={<Navigate to='/404' />} />
      </Route>

      <Route path='/404' element={<NotFound />} />
    </Routes>
  )
}

export default AppRouter
