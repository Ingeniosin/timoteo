import { useEffect, useState } from 'react'
import { getDs } from '../api/api-service'

const useEnum = (api) => {
  const [data, setData] = useState(null)

  useEffect(() => {
    getDs(api).load().then(data => {
      setData({
        loadded: true,
        lookup: (id) => data.find(i => i.id === id)
      })
    })
  }, [api])

  return {
    isLoaded: data?.loadded,
    lookup: data?.lookup
  }
}

export default useEnum
