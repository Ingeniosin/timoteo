namespace Timoteo.Utils; 

public class SecurityUtils {
    public static string HashPassword(string password) {
        return BCrypt.Net.BCrypt.HashPassword(password);
    }
    
    public static bool VerifyPassword(string password, string hashPassword) {
        return BCrypt.Net.BCrypt.Verify(password, hashPassword);
    }
}