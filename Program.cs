using DynamicApi;
using Timoteo.Models;
using Timoteo.Models.Usuarios;
using Timoteo.Services;
using Timoteo.Services.Login;

var builder = WebApplication.CreateBuilder(args);

new DynamicApi<ApplicationDbContext>(builder => {
    builder.addNonService(x => x.EstadosAcademicos);
    builder.addNonService(x => x.InformacionesAcademicas);
    builder.addNonService(x => x.Calendarios);
    builder.addNonService(x => x.TiposCalendarios);
    builder.addNonService(x => x.Cursos);
    builder.addNonService(x => x.Ciudades);
    builder.addNonService(x => x.Aulas);
    builder.addNonService(x => x.Configuraciones);
    builder.addNonService(x => x.DiasSemana);
    builder.addNonService(x => x.Matriculas);
    builder.addNonService(x => x.Actividades);
    builder.addNonService(x => x.FaltaAsistencias);
    builder.addNonService(x => x.Notas);
    builder.addNonService(x => x.CursosProgramas);
    builder.addNonService(x => x.Programas);
    builder.addNonService(x => x.CursosSemestres);
    builder.addNonService(x => x.Semestres);
    builder.addNonService(x => x.Roles);
    builder.addNonService(x => x.TiposIdentificacion);
    builder.addService<Usuario, UsuarioService>(x => x.Usuarios);
    builder.addAction<LoginInput, LoginAction>("login", true);
}, builder).Start();