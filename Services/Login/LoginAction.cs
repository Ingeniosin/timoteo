using DynamicApi.Serializers;
using DynamicApi.Services;
using Microsoft.EntityFrameworkCore;
using Timoteo.Models;
using Timoteo.Utils;

namespace Timoteo.Services.Login; 

public class LoginAction : IActionService<LoginInput> {
    
    private readonly ApplicationDbContext _db;

    public LoginAction(ApplicationDbContext db) {
        _db = db;
    }

    public async Task<object> OnQuery(LoginInput input, HttpContext httpContext) {
        var user = await _db.Usuarios.Include(x => x.Rol).FirstOrDefaultAsync(u => u.Email.ToLower() == input.Email.ToLower());
        
        if (user == null) {
            throw new Exception("El usuario o la contraseña no son correctos");
        }
        
        var isCorrectPassword = SecurityUtils.VerifyPassword(input.Password, user.Password);
        if (!isCorrectPassword) {
            throw new Exception("El usuario o la contraseña no son correctos");
        }
        
        return new { user, };
    }

    public SerializeType SerializeType => SerializeType.STANDARD;
}

public class LoginInput {
    public string Email { get; set; }
    public string Password { get; set; }
}