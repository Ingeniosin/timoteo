using DynamicApi.Services.Listener;
using Timoteo.Models;
using Timoteo.Models.Usuarios;
using Timoteo.Utils;

namespace Timoteo.Services; 

public class UsuarioService : ListenerService<Usuario, ApplicationDbContext> {
    public override async Task OnCreating(Usuario entity, ApplicationDbContext context) {
        entity.Password = SecurityUtils.HashPassword(entity.Password);
    }

    public override async Task OnUpdating(Usuario entity, Usuario oldModel, Func<string, bool> isChanged, ApplicationDbContext context) {
        if(isChanged("password")) {
            entity.Password = SecurityUtils.HashPassword(entity.Password);
        }
    }

    public override async Task Normalize(Usuario entity, ApplicationDbContext context) {
        entity.Email = entity.Email.ToLower().Trim().Replace(" ", "");
        entity.Nombre = entity.Nombre.Trim();
        entity.Apellido = entity.Apellido.Trim();
    }

    public static ListenerConfiguration Configuration => new() {
        OnCreating = true,
        OnUpdating = true,
        Normalize = true
    };
}